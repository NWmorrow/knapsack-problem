"""
Nick Morrow
Matt Horner

Knapsack Problem
(3/16/2018)
"""

from random import *
import time

#WEIGHT
W = int(input("Enter Max Weight: "))
type(W)

#Iterations
n = int(input("Enter number of Iterations: "))
type(n)

randV = [randint(1, W) for i in range(n)]
randW = [randint(1, W/2) for j in range(n)]

items = list(zip(randV, randW))

#Dynamic Programming: K[n-1][W] (Optimal value to be read from)
def knapSack(items, W):
    print("\nDynamic Programming: ")
    print("list of items: " + str(items))

    K = [[0 for x in range(W+1)] for y in range(n+1)]  #Knapsack[n][W+1]


    for i in range(n+1): #Items to choose from in this iteration
        for j in range(W+1):  #Maximum Weight
            if (i == 0 or j == 0):
                K[i][j] = 0
            elif (items[i-1][1] <= j):
                K[i][j] = max(K[i - 1][j], K[i - 1][j - items[i-1][1]] + items[i - 1][0])
            else:
                K[i][j] = K[i-1][j]
    return K[n-1][W]

#Greedy Heuristic (decreasing value)
def greedy1(items, W):
    items = list(reversed(sorted(items, key=lambda x: x[0])))

    print("\nGreedy Heuristic #1: decreasing value")
    print("Sorted list of items: " + str(items))

    K = [0 for x in range(W+1)]  #Knapsack[W+1]

    for j in range(1, W+1):  #Maximum Weight
        iI = 0  #Item Iterator
        ksW = 0  #knapSack Weight
        while (ksW <= j):
            if (iI == n):
                break;
            iVal = items[iI][0]  #item Value
            iW = items[iI][1]  #item Weight
            iI += 1
            if (ksW + iW <= j):  #Check if I have room in my Knapsack
                K[j] += iVal
                ksW += iW
    return K[W]

#Greedy Heuristic (increasing weight)
def greedy2(items, W):
    items = list(sorted(items, key=lambda x: x[1]))

    print("\nGreedy Heuristic #2: increasing weight")
    print("Sorted list of items" + str(items))

    K = [0 for x in range(W+1)]  #Knapsack[W+1]

    for j in range(W+1):  #Maximum Weight
        iI = 0  #Item Iterator
        ksW = 0  #knapSack Weight
        while (ksW <= j):
            if (iI == n):
                break;
            iVal = items[iI][0]  #item Value
            iW = items[iI][1]  #item Weight
            iI += 1
            if (ksW + iW <= j):  #Check if I have room in my Knapsack
                K[j] += iVal
                ksW += iW
    return K[W]

#Calculate Average
def avg(num):
    return float(sum(num)) / max(len(num), 1)

dpTimes = []
optimal = []

g1err = []
g1Times = []

g2err = []
g2Times = []

for i in range(n):
    print("\n----------------------------------- Iteration: " + str(i) + " -----------------------------------\n")
    #DP
    start = time.clock()
    optimal.append(knapSack(items, W))
    end = time.clock()
    dpTimes.append((end - start) * 1000)
    print("Optimal Value: " + str(optimal[i]))
    print("DP Time: " + str(dpTimes[i]))

    #Greedy1
    start = time.clock()
    soln = greedy1(items, W)
    end = time.clock()
    g1Times.append((end - start) * 1000)
    g1err.append(((optimal[i]) - soln)/optimal[i])
    print("Greedy Solution: " + str(soln))
    print("Error: " + str(g1err[i]))
    print("Greedy #1 Time: " + str(g1Times[i]))

    #Greedy2
    start = time.clock()
    soln = greedy2(items, W)
    end = time.clock()
    g2Times.append((end - start) * 1000)
    g2err.append(((optimal[i]) - soln)/optimal[i])
    print("Greedy Solution: " + str(soln))
    print("Error: " + str(g2err[i]))
    print("Greedy #2 Time: " + str(g2Times[i]))

print("\n----------------------------------- Data Results: -----------------------------------\n")

print("Max weight: " + str(W))
print("Nuumber of Items: " + str(n))

print("\nAverage DP Knapsack: " + str(avg(optimal)))
print("DP Average Time: " + str(avg(dpTimes)))

print("\nAverage Error: " + str(avg(g1err)))
print("Greedy #1 Average Time: " + str(avg(g1Times)))

print("\nAverage Error: " + str(avg(g2err)))
print("Greedy #2 Average Time: " + str(avg(g2Times)))
